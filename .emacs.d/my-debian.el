;;;; Debian Helper
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dh_make - Script to Debianize a regular source archive, version 0.46
;;
;;   Usage: dh_make [options]
;;   -c, --copyright <type>    use <type> of license in copyright file
;;                             (gpl|lgpl|artistic|bsd)
;;       --dpatch              using dpatch to maintain patches
;;   -e, --email <address>     use <address> as the maintainer e-mail address
;;   -n, --native              the program is Debian native, don't generate .orig
;;   -f, --file <file>         specify file to use as the original source archive
;;   -r, --createorig          make a copy for the original source archive
;;   -s, --single              set package class to single
;;   -m, --multi               set package class to multiple binary
;;   -l, --library             set package class to library
;;   -k, --kmod                set package class to kernel module
;;   -b, --cdbs                set package class to cdbs
;;   -a, --addmissing          reprocess package and add missing files
;;   -t, --templates <dir>      apply customizing templates in <dir>
;;   -d  --defaultless         skip the default debian and package class templates
;;   -o, --overlay <dir>       reprocess package using template in <dir>
;;   -p, --packagename <name>  force package name to be <name>
;;   -h, --help                display this help screen and exit
;;   -v, --version             show the version and exit

(defun dh_make-debianize (dir) "" (interactive "DWhere: \n")
  (cd dir)
  (pop-to-buffer (format "*DH-Make in %s*" (file-name-nondirectory (expand-file-name dir))))
  (kill-all-local-variables)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (remove-overlays)
  (make-local-variable 'dh_make-command-widgets)
  (dh_make-command-mode)
  (dh_make-command-build-forms)
;;;   (widget-setup)
;;;   (use-local-map widget-keymap)
  (beginning-of-buffer) )
(defvar dh_make-command-widgets nil)
(define-derived-mode dh_make-command-mode widget-minor-mode "dh_make"
  "Docs."
;(defun dh_make-command-defines-widgets () "Defines needed widgets."

  (defsubst get-w-toggles () (get 'dh_make-command-widgets 'options-toggles))
  (defun dh_make-command-option-toggles () "" 
    (push
     (widget-create
      'checklist
      :format "Toggle Options : %v " :entry-format " %b %v"
      '(item "dpatch") '(item "native") '(item "createorig")
      '(item "addmissing") '(item "defaultless"))
     dh_make-command-widgets))
    

  (defsubst get-w-copyright () (get 'dh_make-command-widgets 'options-copyright))
  (defun dh_make-command-option-copyright () ""
    (put 'dh_make-command-widgets 'option-copyright
	 (widget-create
	  'menu-choice
	  :tag "Copyright" :value "gpl" 
	  '(item "gpl") '(item "lgpl") '(item "atristic") '(item "bsd"))))
  
  (defsubst get-w-params () (get 'dh_make-command-widgets 'options-params))
  (defun dh_make-command-option-parameters () ""
    (put
     'dh_make-command-widgets 'option-params      
     (widget-create
      'checklist
      :format "Parameters : \n%v " ;:entry-format "%v"
      '(string :size 20 :format "Email: %v ")
      '(string :format "Package Name: %v\n")
      '(file :format "Original Source Archive: %v\n")
      '(directory :format "Templates Dir: %v\n")
      '(directory :format "Overlays Dir: %v\n"))))

  (defsubst get-w-radios () (get 'dh_make-command-widgets 'options-radios))
  (defun dh_make-command-option-radios () ""
    (put
     'dh_make-command-widgets 'option-parameters      
     (widget-create
      'radio-button-choice
      :format "Select pkg Class : %v " :entry-format " %b %v"
      '(item "single") '(item "multi") '(item "library")
      '(item "kmod") '(item "cdbs"))))
  
  (defun dh_make-command-build-forms () ""
    (widget-insert "\n\n")
    (dh_make-command-option-copyright)
    (widget-insert "\n\n")
    (dh_make-command-option-toggles)
    (widget-insert "\n\n")
    (dh_make-command-option-radios)
    (widget-insert "\n\n")
    (dh_make-command-option-parameters)
    (widget-insert "\n\n"))

  (defun test () ""
    (message " nchilds %d nbutts %d"
	     (length (widget-get (get-w-radios) ':children))
	     (length (widget-get (get-w-radios) ':buttons))))


)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; checkinstall-mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;{{{ checkinstall --help
;;*Seleziona il tipo di Pacchetto*
;; -t,--type=<slackware|rpm|debian> Scegliere il tipo di pacchetto
;; -S                               Costruisce un pacchetto per la Slackware
;; -R                               Costruisce un pacchetto RPM
;; -D                               Costruisce un pacchetto per la Debian
;; *Opzioni di installazione*

;; --install=<yes|no>             Toggle created package installation
;; --fstrans=<yes|no>             Enable/disable the filesystem translation code

;;*Opzioni di Scripting*

;; -y, --default                  Accetta le risposte predefinite a tutte le domande
;; --pkgname=<name>               Descrizione pacchetto
;; --pkgversion=<version>         Descrizione della versione
;; -A, --arch, --pkgarch=<arch>   Imposta l'architettura
;; --pkgrelease=<release>         Indica la versione rilasciata
;; --pkglicense=<license>         Indica la licenza usata
;; --pkggroup=<group>             Imposta il gruppo software
;; --pkgsource=<source>           Imposta il percorso del sorgente
;; --pkgaltsource=<altsource>     Imposta il percorso alternativo del sorgente
;; --pakdir=<directory>           Il nuovo pacchetto verrà salvato nel percorso quì indicato
;; --maintainer=<email addr>      E-mail del curatore del pacchetto (.deb)
;; --provides=<list>              Features provided by this package
;; --requires=<list>              Features required by this package
;; --rpmflags=<flags>             Passa questo flag all'installer rpm
;; --rpmi                         Use the -i flag for rpm when installing a .rpm
;; --rpmu                         Use the -U flag for rpm when installing a .rpm
;; --dpkgflags=<flags>            Passa questo flags all'installer dpkg
;; --spec=<path>                  Locazione del file .spec
;; --nodoc                        Non implementa i file della documentazione

;;}}}





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(load-file (expand-file-name "/usr/share/emacs/site-lisp/debian-startup.el"))
(require 'debian-el)
(require 'dpkg-dev-el)
(require 'develock)
;(require 'devscripts)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'ctypes)
(setq ctypes-file-name (expand-file-name "~/.emacs.d/ctypes.file"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-debian)
