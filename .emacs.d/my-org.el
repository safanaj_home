;; This want to be a replacement of Calendar/Diary/ToDo part of
;; my-pages.
;; Org-mode is integrated in emacs. In my emacs.fanaj (my init-file)
;; Org-mode is automatically turn on for files ending with `.org'

;; In this file i want configure Org and Agenda to learn it.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; I can still to use diary.
(setq org-agenda-include-diary t)
;; less stars
(setq org-hide-leading-stars t)
(setq org-odd-levels-only t)

;; enter follow
(setq org-return-follows-link t)

;; To cooperate with `remember'
(setq org-default-notes-file (expand-file-name (concat user-emacs-directory ".notes")))
(defun view-notes () "" (interactive) (view-file org-default-notes-file))

;; set org files --- XXX ;; (setq org-agenda-files nil)
(setq org-agenda-files
      (cons "~/AGENDA"
	    (append
	     (file-expand-wildcards "~/org/*.org")
	     (file-expand-wildcards "~/emacs/Worg/worg-todo.org"))
	    ))

;(add-to-list 'org-agenda-files (list my-org-agenda-files))

;; To test ...
(require 'facemenu)
(load (expand-file-name "~/gnu-src/onVersion/Emacs/emacswikicode/org-annotate-file.el") t)

;; org-publish and org-blog to achieve to make org-wiki
(load (expand-file-name "~/gnu-src/onVersion/Emacs/emacswikicode/org-publish.el") t)
(load (expand-file-name "~/gnu-src/onVersion/Emacs/emacswikicode/org-blog.el") t)



;; Key Bindings Map ... o stands for Org,
;; but O is for `org-agenda' directly
(define-key my-prefix-map "O" 'org-agenda)
(define-prefix-command 'my-org-map)
(define-key my-prefix-map "o" 'my-org-map)
(define-key my-org-map "l" 'org-store-link)

;;;;;;;;;;;;;;;;;
(provide 'my-org)


;;;; Other stuff ... diary, page , ...for writing

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For Calendar/Diary  &  TODOo
(add-hook 'diary-display-hook 'fancy-diary-display) ;; (setq diary-display-hook nil)
(setq european-calendar-style t)
(add-hook 'list-diary-entries-hook 'sort-diary-entries t)
;(setq todoo-file-name "~/TODO")
;(setq todo-file-do "~/TODOit")
;todo-file-top

