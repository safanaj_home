;; (add-to-list 'load-path "/usr/share/emacs21/site-lisp/w3-el")
;; (add-to-list 'load-path "/usr/share/emacs21/site-lisp/url")

(defvar me21sl-dir "~/.emacs.d/e21links" "")
(defvar me21sl-dirs '("/usr/share/emacs21/site-lisp/w3-el")
  "list of files to symlink in .emacs.d")

(defun my-e21-site-lisp-update-sym-lynks ()
  ""
  (interactive)
  (mapcar
   (lambda (file)
     (shell-command (concat "ln " file " " me21sl-dir "/")))
   (split-string
    (shell-command-to-string
;; FIX (car dirs), with a mapcar ..
     (concat "ls -1 " (car me21sl-dirs) "/*.el" )))))

(defun my-e21-site-lisp-remove-sym-lynks ()
  ""
  (interactive)
  (mapcar
   (lambda (file)
     (shell-command (concat "rm " me21sl-dir "/" (basename file))))
   (split-string
    (shell-command-to-string
;; FIX (car dirs), with a mapcar ..
     (concat "ls -1 " (car me21sl-dirs) "/*.el" )))))

(add-to-list 'load-path (expand-file-name "~/emacs.d/e21links"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-e21-site-lisp)
