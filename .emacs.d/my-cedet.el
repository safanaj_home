;;;;
;;
;;{{{ Load CEDET from CVS directory ...

;; may be install it in /gnu/... in modo automatico ???
(defconst cedet-cvs-dir-for-load "/gnu/src/onVersion/Emacs/cedet" "")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/common"))
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/eieio"))
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/ede"))
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/cogre"))
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/srecode"))
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/quickpeek"))
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/semantic"))
(add-to-list 'load-path
	     (concat (expand-file-name cedet-cvs-dir-for-load) "/contrib"))

;(if (= (string-to-number emacs-version) 23)
;    (load-file "/common/cedet.el"))
;(load-file (concat (expand-file-name cedet-cvs-dir-for-load) "/common/cedet.el")

;;}}}
(setq semantic-load-turn-useful-things-on t)
(require 'cedet)

;(load-file (concat cedet-cvs-dir-for-load "/cedet-update-version.el"))
;; Loading
(require 'eieio-load)
(require 'ede-load)
(require 'semantic-load)
(require 'srecode-load)  ;;(@file :file-name "~/cedet-src/srecode/" :to "~/cedet-src/srecode/" :display "SRecorder")
;(require 'cogre-load)
(require 'cogre)
; for ede-gnustep
(require 'cedet-contrib-load)
(require 'cedet-contrib)


;;;; Test EIEIO
;(defclass j-test () nil "...")


;;{{{ ECB Stuff 

(add-to-list 'load-path (expand-file-name "/gnu/src/onVersion/Emacs/ecb"))
(require 'ecb)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun toggle-ecb ()""(interactive) (if ecb-minor-mode (ecb-toggle-ecb-windows)(ecb-activate)))

;;}}}
;;{{{ EDE Stuff
;(require 'ede-simple)
;; ede-project-class-files
;; ede-gnustep-root-project-list
;; (add-to-list 'ede-project-class-files
;; 	     (ede-project-autoload "gnustep-root"
;; 	      :name "GNUstep-style Project ROOT"
;; 	      :file 'ede-gnustep-root
;; 	      :proj-file 'ede-gnustep-root-project-file-for-dir
;; 	      :proj-root 'ede-gnustep-root-project-root
;; 	      :load-type 'ede-gnustep-root-load
;; 	      :class-sym 'ede-gnustep-root)
;; 	     t)


;;}}}

;;;; Key Bindings
(define-prefix-command 'my-cedet-map)
(define-key my-cedet-map ",p" 'ede-speedbar)
;(define-key my-cedet-map ",e" 'eieio-speedbar)
(define-key my-cedet-map ",a" 'semantic-speedbar-analysis)
;; in my-prefix-map
(define-key my-prefix-map "." 'my-cedet-map)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Configuring *CEDET*
;;

(add-to-list 'auto-mode-alist '("\\.wy$" . wisent-grammar-mode))


;;; C-c /   è scomodo 
(defvar my-srecode-prefix-key [(control ?c) ?-]
  "The common prefix key in srecode minor mode.")

(defvar my-srecode-prefix-map
  (let ((km (make-sparse-keymap)))
    ;; Basic template codes
    (define-key km "-" 'srecode-insert)
    (define-key km "." 'srecode-insert-again)
    (define-key km "E" 'srecode-edit)
    ;; Template indirect binding
    (let ((k ?a))
      (while (<= k ?z)
	(define-key km (format "%c" k) 'srecode-bind-insert)
	(setq k (1+ k))))
    km)
  "Keymap used behind the srecode prefix key in in srecode minor mode.")


(setq srecode-minor-mode-hook nil)
(add-hook 'srecode-minor-mode-hook
	  (lambda()
	    (setq srecode-mode-map
		  (let ((km (make-sparse-keymap)))
		    (define-key km my-srecode-prefix-key my-srecode-prefix-map)
		    km))
	    t))
	  

(global-semantic-idle-scheduler-mode 1)
;(semantic-load-enable-excessive-code-helpers)
(set-variable 'semanticdb-cscope-program "cscope")
(global-ede-mode 1)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-cedet)

;;;; My stuff future libs
;;{{{ Contol Panel

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (require 'widget)
(require 'wid-edit)
(require 'eieio)

(defclass CP-simple-form-builder ()
  ((widgets-stuff :initarg :widgets-stuff :initform nil
		  :type list
		  :custom (repeat
			   (choice (text :tag "Literal")
				   (list :tag "Toggles" (repeat string))
				   (list :tag "Editable List items tag" (repeat string))))
		  :documentation
		  "")
   )
  "Class CP-simple-form-builder ")

;(customize-object (CP-simple-form-builder "Caio"))

;;; Command CP Builder

(require 'eieio)
(defclass CP-cmd-line-options ()
  ((mandatory :initarg :mandatory :initform nil :type boolean
	      :custom checkbox
;	      :custom (choice (item :tag "on" t) (item :tag "off" nil))
	      :documentation
	      "")
   (opt-type :initarg :option-type :initform nil :type symbol
	     :custom (choice (const toggle)(const params)(const radios))
	     :documentation
	     "")
   (opt-sym :initarg :opt-sym :initform nil :type symbol :custom symbol
	    :documentation
	    "")   
   (opt-val :initarg :opt-val :initform "" :type string :custom string
	    :documentation
	    "")   
   (opt-doc :initarg :opt-doc :initform "" :type string :type string
	    :documentation
	    "")
   )
  "Class CP-command-option ")
  
(defclass CP-cmd-builder ()
  ((command :initarg :command :type string :custom string)
   (options :initarg :options :type list
	    :custom CP-cmd-line-options)
   (widgets :initarg :widgets :type list)
   (layout :initarg :layout
	   :initform nil
	   :type list
	   :documentation
	   ""))
  "Doc Class")

;; (defmethod get-string-cmd ((o CP-cmd-builder)) ""
;;   (let ((result (oref o command))
;; 	(opts (oref o options)))
;;     (while (car opts)
;;       (setq result
;; 	    (concat result " "
;; 		    (cond ((eq 'toggle (car (car opts))) (symbol-name (cadr (car opts))))
;; 			  ((eq 'params (car (car opts)))
;; 			   (concat (symbol-name (cadr (car opts))) " \"" (caddar opts) "\""))
;; ;; 			  ((eq 'radios (caar opts))
;; ;; 			   (concat 
;; 			)))
;;       (setq opts (cdr opts)))
;;     result))

;; (defmethod build-form ((o CP-cmd)) ""
;;   )

;; (setq W (widget-convert
;; 	 'checklist
;; 	 :format "Toggle Options : %v " :entry-format " %b %v"
;; 	 '(item "dpatch") '(item "native") '(item "createorig")
;; 	 '(item "addmissing") '(item "defaultless")))

 
;; (setq OBJ (CP-cmd-line-options "OBJ"))
;; (setq BOBJ (CP-cmd-builder "BOBJ"))

;; (eieio-customize-object OBJ)
;; (eieio-customize-object BOBJ)

;; (get-string-cmd OBJ)


;; (defclass control-panel-command-class ()
;;   ((command :initarg :command
;; 	    :type string
;; 	    :documentation "")
;;    (argumets :initarg :arguments
;; 	     :type (repeat string)
;; 	     :documentation "")
;;    (options :initarg :options
;; 	    :type  (repeat
;; 		    (list
;; 		     (choice (const 'toggle)
;; 			     (const 'params)
;; 			     (const 'radios))
;; 		     symbol string string)
;; 	    :documentation "")))
;;  "Class to wrap command shell tools.")

;; (defmethod get-command-string ((this control-panel-command-class))
;;   (let ((opts (oref this options)) cmdstring)
;;     (setq cmdstring (concat (oref this command)))
;;     (while (opts)
;;       (cond ((and (eq (caar opts) 'toogle)
;; 	     (setq cmdstring (concat cmdstring " " (cadar opts))))
;; 	    ((eq (caar opts) 'radios)
;; 	     (setq cmdstring (concat cmdstring " " (cadar opts)))))
;;       (setq opts (cdr opts))))))

;; (eieio-customize-object
;;  (control-panel-command-class "test" :command "dh_make" :arguments "nil"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;}}}

