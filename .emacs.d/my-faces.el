;; Font sizes
(defvar font-resizing-mode-map
  (let ((m (make-sparse-keymap)))
    (define-key m "+" 'increment-font-height)
    (define-key m "-" 'decrement-font-height)
    (define-key m "r" 'resize-font-height)
;    (define-key m "-" 'set-font-height)
    (define-key m (kbd "C-c C-c") 'font-resizing-mode)
    m))

(define-minor-mode font-resizing-mode
  :init-value nil
  :ligther " Font re-Size "
  :keymap 'font-resizing-mode-map)
(put 'font-resizing-mode 'unit 8)

;; (defun set-font-height (&optional n) "" (interactive "p")
;;   (set-face-attribute 'default (selected-frame)
;; 		      :height (+ (face-attribute 'default :height) n))
;;   (princ (format "Font height is %d." (face-attribute 'default :height))))

(defun resize-font-height (&optional n) "" (interactive "p")
  (set-face-attribute 'default (selected-frame)
		      :height (+ (face-attribute 'default :height) n))
  (princ (format "Font height is %d." (face-attribute 'default :height))))

(defsubst decrement-font-height ()""(interactive)
  (resize-font-height (- (get 'font-resizing-mode 'unit))))
(defsubst increment-font-height ()""(interactive)
  (resize-font-height (get 'font-resizing-mode 'unit)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun switch-inverted-color () "Switch Inverted Color Video."
  (interactive) (message "Switch Inverted Color Video.")
  (set-face-inverse-video-p 'default (not (face-inverse-video-p 'default))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(defvar my-color-style nil "Color Style")
(defun switch-color-style ()
 " (interactive)
  (if (eq style 'dark)
      (prog1 (setq style nil)
	      (custom-set-faces
	       '(default ((t (:background \"white\" :foreground \"black\"))))))
    (prog1 (setq style 'dark)
      (custom-set-faces
       '(default ((t (:background \"black\" :foreground \"white\"))))))))
"
 (interactive) (message "Switch Color.")
 (if (eq (get 'switch-color-style 'style) 'dark)
     (prog1 (put 'switch-color-style 'style 'light)
       (set-face-attribute 'default (selected-frame) :background "white" :foreground "black"))
   (prog1 (put 'switch-color-style 'style 'dark)
     (set-face-attribute 'default (selected-frame) :background "black" :foreground "white")))
 (message "Switched Color to %s." (symbol-name (get 'switch-color-style 'style))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun switch-font-style (&optional style) ""
  (interactive)
  (message "Switch Font Size.")
  (if (null style) (setq style
			 (intern 
			  (completing-read 
			   "Font Style: "
			   (get 'switch-font-style 'avails)))))
  (set-face-attribute 'default (selected-frame)
		      :height (get 'switch-font-style style))
  (message "Switched Font Size to %s." style))

(put 'switch-font-style 'avails '((tiny) (smaller) (bigger) (huge)))
(put 'switch-font-style 'huge 182)   ;;  -5    x8      
(put 'switch-font-style 'bigger 143) ;;  -5    x8
(put 'switch-font-style 'smaller 103);;  -3    x8
(put 'switch-font-style 'tiny 79)    ;;        x8

;;;; Various Faces
(defface my-Corsivo-sottolineato-rosso
  '((t (:family "LMSans17"
	:underline t
	:slant oblique
	:underline t
	:foreground "red"
	:inherit default)))
  ""
  :group 'my)

(defface my-Titolo-enorme
  '((t (:family "LMSans9"
	:height 4.0
	:slant oblique
	:weight bold
	:inherit default)))
  ""
  :group 'my)

;; (defface my-face
;;   '((t
;;      ( :stipple nil 
;;        :background "white" 
;;        :foreground "black" 
;;        :inverse-video nil 
;;        :box nil
;;        :strike-through nil 
;;        :overline nil 
;;        :underline nil 
;;        :slant normal 
;;        :weight normal 
;;        :height 87 
;;        :width normal 
;;        :family "b&h-lucida"))) "My Face")

;;;;;;;;;;;;;;;; For Various & Hooks ;;;;;;;;;;;;;;;;
;;;; For Term
(defun update-face-for-term () "" (interactive)
  (custom-set-variables
   '(term-default-bg-color (face-background 'default))
   '(term-default-fg-color (face-foreground 'default))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-faces)
