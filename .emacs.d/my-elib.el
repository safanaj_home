
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; TO USE EMACS
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Maintain a good .emacs.d

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Compile ~/.emacs.d/*.el ...
(defun byte-compile-in-user-emacs-directory (&optional not-all) "" (interactive "p")
  (let ((files)
	(_cmd (if (> not-all 0) "ls -1 *.el |grep -v -e '^my\\|^mail-and-gnus\\|^making'"
	       "ls -1 *.el")))
    (save-excursion
      (cd user-emacs-directory)
      (setq files (split-string (shell-command-to-string _cmd)))
      (while (car files)
	(byte-compile-file (car files))
	(setq files (cdr files))))))

(defun clean-user-emacs-dir ()""(interactive)
  (shell-command (concat "rm " user-emacs-directory "/*.elc")))

(defun archive-user-emacs-dir ()""
  (interactive)(cd user-emacs-directory)
  (shell-command "tar czf ../dotEmacs.tar.gz ."))

 ;; Various only emacs-dependent (elisp libs by GNU Emacs)
;; cool
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Show/Hide the other window (window-list)
(defun show/hide-other-window () "Show/Hide the other window" (interactive)
  (if (< 1 (length (window-list)))
      (delete-other-windows)
    (display-buffer (get-next-valid-buffer (buffer-list) (current-buffer)) t)))

(defun show/switch-other-window ()
  "Show/Switch buffer in other window."
  (interactive)
  (if (< 1 (length (window-list))) ; more windows
	(progn
	  (or (boundp 'idx-bow)
	      (setq idx-bow 2))
	  (display-buffer (get-next-valid-buffer
			   (buffer-list)
			   (nth idx-bow (buffer-list))) t)
	  (if (< idx-bow (length (buffer-list)))
	      (setq idx-bow (1+ idx-bow))
	    (setq idx-bow 2)))
    (progn (setq idx-bow 2)
	   (display-buffer
	    (get-next-valid-buffer (buffer-list) (current-buffer)) t))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Show/Hide messages in other window
(defun messages-is-visible-p ()""(get-buffer-window-list "*Messages*"))
(defun show/hide-messages ()"Show/Hide messages in other window"(interactive)
  (if (messages-is-visible-p)
      (progn
	(select-window (car (messages-is-visible-p)))
	(bury-buffer)(delete-window))
;else;    (display-buffer "*Messages*")))
    (progn
      (switch-to-buffer-other-window "*Messages*" t)
      (goto-char (point-max)) (other-window 1))))

(defcustom minimal-height-for-window 4 "...")
(defcustom minimal-width-for-window  10 "...")
(defun minimize-window () ""
  (cond ((eq (window-width) (frame-width))
	 (enlarge-window (- minimal-height-for-window (window-height))))
	((eq (1+ (window-height)) (frame-height))
	 (enlarge-window (- minimal-width-for-window (window-width)) t))
;;; 	((not
	(t
	 (progn
	   (unless (and (cdr (window-list))
			(eq (window-height (next-window))
			    (window-height)))
	     (enlarge-window (- minimal-width-for-window (window-width)) t))
	   (unless (and (cadr (window-list))
			(eq (window-width ())
			    (window-width)))
	     (enlarge-window (- minimal-height-for-window (window-height))))))
	))

(defun maximize-window ()
  ""
  (interactive)
  (enlarge-window (- (frame-height) (1+ minimal-height-for-window) (window-height)))
  (enlarge-window (- (frame-width) (1+ minimal-width-for-window) (window-width)) t))
  
(defun other-window-minimizing ()""(interactive) (minimize-window)(other-window 1))
(defun other-window-maximizing ()""(interactive) (other-window 1)(maximize-window))



;;;; Utils to write
(defun title-page-at (title &optional pos)
  (interactive "sTitle: \nd") (unless pos (setq pos (point)))
  (goto-char pos)
  (insert (concat " " title)))

(defun title-page (title)(interactive)
  (beginning-of-buffer)(title-page-at title))

;; Utils
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; xsetroot ...
(defun xsetroot (img-file) "" (interactive "fSelect the Image: ")
  (shell-command (concat "xli -onroot -fullscreen " (shell-quote-argument img-file))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; chomp `perl-like' function
(defun chomp (str)
  "chomp `perl-like' function. Take a string or symbol
and return a string without final or initial whitespace class chars."
  (let ((s (if (symbolp str)(symbol-name str) str)))
    (save-excursion
      (while (and
	      (not (null (string-match "^\\( \\|\f\\|\t\\|\n\\)" s)))
	      (> (length s) (string-match "^\\( \\|\f\\|\t\\|\n\\)" s)))
	(setq s (replace-match "" t nil s)))
      (while (and
	      (not (null (string-match "\\( \\|\f\\|\t\\|\n\\)$" s)))
	      (> (length s) (string-match "\\( \\|\f\\|\t\\|\n\\)$" s)))
	(setq s (replace-match "" t nil s))))
    s))

;; (get-buffer-process (get-buffer "*scratch*"))
;; (get-buffer-window (current-buffer))
;; (get-buffer-window-list (current-buffer))
;; (get-buffer-window-list (get-buffer "*scratch*"))

;; Requires some libs in my .emacs.d
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For Proced ...
(require 'proced)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; For Common Lisp ...
(setq inferior-lisp-program "/usr/local/bin/sbcl --noinform")
(require 'slime)
(slime-setup)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For Git ...
(require 'vc-git)
(when (featurep 'vc-git) (add-to-list 'vc-handled-backends 'git))
(require 'git)
(autoload 'git-blame-mode "git-blame" "blah blah" t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; for HTTP stuff
(require 'http-cookies nil t)
(require 'http-get nil t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-elib)
