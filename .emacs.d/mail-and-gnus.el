;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Mail setting
(setq user-mail-address "bardelli.marco@gmail.com")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq smtpmail-default-smtp-server "smtp.gmail.com")
;(setq smtpmail-default-smtp-server "ssl.cs.unibo.it")
(require 'smtpmail)
(require 'gnus)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; for Gnus ...
;; Settings ...
(message "Gnus Settings ... ")
;; (setq gnus-secondary-select-methods
;;       '((nntp "allnews.readfreenews.net")
;; 	(nntp "pubnews.gradwell.net")
;; 	(nntp "news.gmane.org")))

;      gnus-secondary-servers '("allnews.readfreenews.net" "freenews.netfront.net" "textnews.news.cambrium.nl" "news.f.de.plusline.net" "dp-news.maxwell.syr.edu" "pubnews.gradwell.net" "news.vsi.ru" "news.grc.com" "w3bhost.de" "news.amu.edu.pl")

(setq gnus-select-method '(nndraft))
;(setq gnus-select-method '(nntp "news.gmane.org"))
;(setq gnus-secondary-select-methods '())
;; (setq gnus-secondary-select-methods '(;(nntp "news.linuxfan.it")
;; 				      (nntp "nnrp-beta.newsland.it")))

(setq gnus-asynchronous t)
(setq gnus-summary-line-format "%U%R%z%d %I%(%[ %F %] %s %)\n")
;(setq gnus-summary-line-format "%U%R%z%I%(%[%4L: %-23,23f%]%) %s\n")



;; gnus-group-mode-hook
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

(message "done.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; for smtpmail ... Sending Mail ...
;; Ex:
;; Configure outbound mail (SMTP)
(setq mail-account-list '("google"))
(setq mail-account-active (car mail-account-list)) ;; default google ...

(defun setup-smtpmail ()
  "Setup Mail account to send mail."
  (interactive)
  (if (string= "unibo" mail-account-active)
      (setq smtpmail-starttls-credentials '(("ssl.cs.unibo.it" 587 nil nil))
	    smtpmail-smtp-server "ssl.cs.unibo.it"
	    smtpmail-default-smtp-server "ssl.cs.unibo.it"
	    send-mail-function 'smtpmail-send-it
	    message-send-mail-function 'smtpmail-send-it
	    smtpmail-smtp-service 465 ;; 587
	    smtpmail-auth-credentials '(("ssl.cs.unibo.it"
					 465 ;; 587
					 "bardelli@cs.unibo.it"
					 (chomp
					  (shell-command-to-string
					   (concat
					    "grep jbook "
					    (expand-file-name "~/.passwords")
					    " | cut -d':' -f2"))))))
    t)
  (if (string= "google" mail-account-active)
      (setq smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
	    smtpmail-smtp-server "smtp.gmail.com"
	    smtpmail-default-smtp-server "smtp.gmail.com"
	    send-mail-function 'smtpmail-send-it
	    message-send-mail-function 'smtpmail-send-it
	    smtpmail-smtp-service 587
	    smtpmail-auth-credentials '(("smtp.gmail.com"
					 587
					 "bardelli.marco@gmail.com"
					 (chomp
					  (shell-command-to-string
					   (concat
					    "grep google "
					    (expand-file-name "~/.passwords")
					    " | cut -d':' -f2"))))))
    t))

(message "Setup SMTP Mail (%s) ..." mail-account-active)
(setup-smtpmail)
(message "done.")
;;;;

;; (defun setup-smtpmail-switched () "..."
;;   (interactive)
;;   (prog1
;;       (if (string= "unibo" mail-account-active)
;; 	  (setq mail-account-active (elt mail-account-list 1))
;; 	(setq mail-account-active (elt mail-account-list 0)))
;;     (setup-smtpmail))
;;   (message mail-account-active))

(setq smtpmail-debug-info t)
(setq smtpmail-debug-verb t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Send Mail ...
;; (setq send-mail-function 'sendmail-send-it)
;; (setq message-send-mail-function 'sendmail-send-it)
(setq send-mail-function 'smtpmail-send-it)
(setq message-send-mail-function 'smtpmail-send-it)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Read Mail ...
(require 'rmail)
(require 'rmail-extras (expand-file-name
			(concat user-emacs-directory "elisp/rmail-extras")) t)
(require 'rmailgen (expand-file-name
		    (concat user-emacs-directory "elisp/rmailgen")) t)
 ;;; to comment for 23.0.60 ... ?? fixed error ... i colori ???
(require 'rmime
	 (expand-file-name
	  (concat user-emacs-directory "elisp/rmime.el")) t)


;; (add-hook 'rmail-show-message-hook 'rmime-format)
;; (add-hook 'rmail-edit-mode-hook    'rmime-cancel)
;(autoload 'rmime-format "~/.emacs.d/elisp/rmime.el" "" nil)

(require 'etach)
(set-variable 'etach-debug t)
(set-variable 'etach-detachment-default-directory (expand-file-name "~/Mail/detached/"))
(set-variable 'etach-detachment-discard-directory (expand-file-name "~/Mail/discarded/"))
;;(set-variable 'etach-restore-buffer-after-detach t)
;;;;
;(setq rmail-file-name "~/RMAIL")
(setq rmail-primary-inbox-list
      ;;nil)
      '("/var/mail/fanaj" ))

;; decoded mime not now ...
;(eval-expression
(setq rmail-enable-mime t)
(setq rmail-redisplay-summary t) ;)
(if (featurep 'rmime) (add-hook 'rmail-mode-hook 'rmime-mode))


(setq rmail-remote-password-required t)
;(setq rmail-movemail-flags '("--tls"))
(setq rmail-preserve-inbox nil)
(setq rmail-nonignored-headers
      (concat rmail-nonignored-headers "\\|^list-id:"))

(defun rmail-or-rmime () "" (interactive)
  (if (string= (buffer-name (current-buffer)) "RMAIL") (rmime-mode)
    (rmail)))

(defun rmail-other-frame () "" (interactive)
  (add-hook 'after-make-frame-functions '(lambda (f)
					   (select-frame f)
					   (rmail)))
  (make-frame '((name . "RMAIL") (minibuffer . nil)))
  (remove-hook 'after-make-frame-functions '(lambda (f)
					      (select-frame f)
					      (rmail))))
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Check Mails ... & get them
(defun check-messages () "check /var/mail/fanaj for messages"(interactive)
  (message (substring (shell-command-to-string "messages") 0 -1)))

(defun check-mails () "check mail with fetchmail"(interactive)
  (shell-command-in-console "fetchmail -c -k &"))

(defun get-mails ()""(interactive)(shell-command "fetchmail -v -U"))
(defun get-keeped-mails ()""(interactive)(shell-command "fetchmail -v -k -U"))
(defun get-all-mails ()""(interactive)(shell-command "fetchmail -v --all"))
(defun kill-fetchmail ()"" (interactive)(shell-command "fetchmail -v -q"))
(defun mails-headers ()"" (interactive)(shell-command "mail -H"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; (defun fetchmail ()
;; ;;   "First polls server with fetchmail -c and reports status,
;; ;;  then offers to run fetchmail without any switches."
;; ;;   (interactive)
;; ;;   (message "checking for mail...")
;; ;;   (let ((check (shell-command-to-string "fetchmail -c")))
;; ;;     (if (y-or-n-p
;; ;; 	 (concat
;; ;; 	  (cond ((string-match "failed" check) "failed to poll server")
;; ;; 		((string-match "No mail" check) "no messages to fetch")
;; ;; 		((string-match "message" check)
;; ;; 		 (concat
;; ;; 		  (substring check 0 (string-match " " check))
;; ;; 		  " messages to fetch "
;; ;; 		  (substring check
;; ;; 			     (string-match "(" check)
;; ;; 			     (+ 1 (string-match ")" check)))))
;; ;; 		(t "terribly confused"))
;; ;; 	  " - run fetchmail? "))
;; ;; 	(progn (message "Running fetchmail...")
;; ;; 	       (let ((error-code (call-process "fetchmail")))
;; ;; 		 (message
;; ;; 		  (concat "Running fetchmail... "
;; ;; 			  (cond ((eq error-code 0) "got mail")
;; ;; 				((eq error-code 1) "no mail")
;; ;; 				(t "some kind of error occurred"))))))
;; ;;       (message nil))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BBDB ...
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'message)
(add-to-list 'load-path "/usr/share/emacs/site-lisp/bbdb/lisp")
(setq bbdb-file (concat (expand-file-name user-emacs-directory) "bbdb.file"))
(require 'bbdb)
(bbdb-initialize 'rmail 'gnus 'message)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Hooks

(add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)
(add-hook 'rmail-mode-hook 'bbdb-insinuate-rmail)
;;(bbdb-insinuate-message)
(add-hook 'mail-mode-hook 'message-mode)
(add-hook 'mail-mode-hook 'bbdb-insinuate-message)

;; (add-hook 'rmail-show-message-hook 'rmime-format)
;; (add-hook 'rmail-edit-mode-hook    'rmime-cancel)
;; (add-hook 'rmail-mode-hook 'rmime-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Archive Mails
;; from summary ...
;; ... ricorda `C-u 10 o' archivia 10 mail da summary

(setq rmail-automatic-folder-directives
      '(
	;; Silvia
	("~/Mail/MISS" "from" "miss.?silvia86")

	;; shhshhhshh ... parla jbook
	("~/Mail/from_jbook" "from" "@jbook.sana.figa")

	("~/Mail/PayPal" "from" "assistenza@paypal.it")
	;; università
	("~/Mail/UNIBO" "from" "bardelli@cs.unibo.it")
	("~/Mail/UNIBO" "cc" "bardelli@cs.unibo.it")
	("~/Mail/UNIBO" "from" "@studio.unibo.it")
	("~/Mail/UNIBO" "from" "servizio.laureati@almalaurea.it")
	;; as usual
	("~/Mail/GNUstep.buggies" "list-id" "bug-gnustep.gnu.org")
	("~/Mail/bugs" "subject" "\\[bug #")
	("~/Mail/GNUstep" "from" "gnustep")
	("~/Mail/GNUstep" "cc" "gnustep")
	("~/Mail/FSFE" "list-id" "press-release-it.fsfeurope.org")
	("~/Mail/BFSF" "list-id" "bfsf.liste.bfsf.info")
	("~/Mail/INFO-FSF" "list-id" "info-fsf.gnu.org")

	;;; varie liste
	;; CEDET
	("~/Mail/Lists/cedet-devel" "list-id" "cedet-devel.lists.sourceforge.net")
	("~/Mail/Lists/cedet-semantic" "list-id" "cedet-semantic.lists.sourceforge.net")
	;; Grillo
	("~/Mail/Lists/grillo" "subject" "La Rete del Grillo")
	("~/Mail/Lists/grillo" "from" "info@meetup.com")
	;; GNU-It
	("~/Mail/Lists/gnuit" "subject" "\\[gnuit-list")
	("~/Mail/Lists/gnuit" "list-id" "gnuit-list.it.gnu.org")
	;; Emacs
	("~/Mail/Lists/emacs-commit" "list-id" "emacs-commit.gnu.org")
	("~/Mail/Lists/emacs-devel" "list-id" "emacs-devel.gnu.org")
	("~/Mail/Lists/emacs-help" "list-id" "help-gnu-emacs.gnu.org")
	("~/Mail/Lists/emacs-info" "list-id" "info-gnu-emacs.gnu.org")
	;; Debian
	("~/Mail/Lists/debian-live-devel" "list-id" "debian-live-devel.lists.alioth.debian.org")
	;; Grillo
	("~/Mail/Lists/grillo" "list-id" "beppegrillo-56.meetup.com")
	;; RELug
	("~/Mail/Lists/relug" "list-id" "relug.lists.linux.it")

	;; Laovoro IOR
	("~/Mail/IOR" "from" "@ior.it")

	;; SPAM
	("/dev/null" "subject" "Poste Italiane\\|Rolex\\|Replica\\|Discount")
	("/dev/null" "subject" "meds\\|pharmacy\\|forza per il pene\\|penis\\|luxury")
	("/dev/null" "subject" "avere il sesso\\|free bonus")
	("/dev/null" "to" "barbaros@cs.unibo.it\\|barbieri@cs.unibo.it")
	("/dev/null" "from" "Banca \\(di \\)?Roma\\|BancoPost[ea]\\|Poste Italiane")

	("/dev/null" "subject" "\\[Suspected Spam\\]")
	("/dev/null" "subject" "\\*\\*\\*\\*\\*SPAM\\*\\*\\*\\*\\*")

	;; Poste Mail
	("~/Mail/@POSTE.it" "to" "marco.bardelli@poste.it")
	))



(provide 'mail-and-gnus)
