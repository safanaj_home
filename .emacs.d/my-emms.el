;; EmmS ... m for Music ...
(require 'emms-setup)
(require 'emms-metaplaylist-mode)
(require 'emms-mode-line)
(require 'emms-playing-time)
(require 'emms-browser)

(setq emms-source-file-default-directory "~/Music/")
(setq emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)
(setq emms-playlist-buffer-name "*Music*")
(setq emms-playlist-default-major-mode 'emms-playlist-mode)

(setq emms-player-gstreamer-parameters nil)
(setq emms-player-list '(emms-player-gstreamer))

(defun emms-sources-update (&optional dir regex)
  "Run emms-source-file-directory-tree in emms-source-file-default-directory"
  (interactive) ; "sDirectory: \nsRegex: ")
  (if (not dir) (setq dir
		      (read-directory-name "Sources in directory: " emms-source-file-default-directory nil t)))
  (if (not regex) (setq regex
			(read-string "Regex for tracks: ")))
  (if (empty-string-p dir) (setq dir emms-source-file-default-directory))
  (if (empty-string-p regex) (setq regex ".*"))
  (save-excursion
    (let ((old (car (emms-playlist-buffer-list))))
      (emms-playlist-set-playlist-buffer (emms-playlist-new-with-name "%- Last Added -%"))
      (dolist (file-track (emms-source-file-directory-tree dir regex))
	(emms-add-file file-track))
      (message "Sources Updated")
      (emms-playlist-set-playlist-buffer old))))

(defun emms-browser-regenerate () "Kill and regenerate EMMS Browser" (interactive)
  (save-excursion
    (emms-browser)(kill-buffer (current-buffer))(emms-browser)))



(defun emms-info-repeating () "" (interactive)
  (if emms-repeat-track (message "Repeat Track.")
    (if emms-repeat-playlist (message "Repeat Playlist.")
      (message "Not Repeat Nothing."))))

(defun emms-toggle-repeat-playlist () "" (interactive)
  (setq emms-repeat-playlist (not emms-repeat-playlist))
  (emms-info-repeating))

(defun emms-toggle-repeat-track () "" (interactive)
  (setq emms-repeat-track (not emms-repeat-track))
  (emms-info-repeating))

(defun emms-info-current-track () "" (interactive)
  (let ((msg "%s in %s da %s del %d -- track nr. %d dura %d\n"))
    (message msg
	     (emms-track-get (emms-playlist-current-selected-track) 'info-artist)
	     (emms-track-get (emms-playlist-current-selected-track) 'info-title)
	     (emms-track-get (emms-playlist-current-selected-track) 'info-album)
	     (if (stringp (emms-track-get (emms-playlist-current-selected-track) 'info-year))
		 (string-to-int (emms-track-get
				 (emms-playlist-current-selected-track) 'info-year))
	       (if (emms-track-get (emms-playlist-current-selected-track) 'info-year)
		   (emms-track-get (emms-playlist-current-selected-track) 'info-year) 0))
	     (if (stringp (emms-track-get
			   (emms-playlist-current-selected-track) 'info-tracknumber))
		 (string-to-int (emms-track-get
				 (emms-playlist-current-selected-track) 'info-tracknumber))
	       (if (emms-track-get (emms-playlist-current-selected-track) 'info-tracknumber)
		   (emms-track-get (emms-playlist-current-selected-track) 'info-tracknumber) 0))
	     (if (stringp (emms-track-get
			   (emms-playlist-current-selected-track) 'info-playing-time))
		 (string-to-int (emms-track-get
				 (emms-playlist-current-selected-track) 'info-playing-time))
	       (if (emms-track-get (emms-playlist-current-selected-track) 'info-playing-time)
		   (emms-track-get (emms-playlist-current-selected-track) 'info-playing-time) 0)))))
	     
    

;; (emms-tag-editor- (emms-playlist-current-selected-track))
;; (emms-track-get (emms-playlist-current-selected-track) 'info-artist)
;; (emms-track-get (emms-playlist-current-selected-track) 'info-title)
;; (emms-track-get (emms-playlist-current-selected-track) 'info-album)
;; (emms-track-get (emms-playlist-current-selected-track) 'info-year)
;; (emms-track-get (emms-playlist-current-selected-track) 'info-tracknumber)
;; (emms-track-get (emms-playlist-current-selected-track) 'info-playing-time)

(defun emms-my-add () "" (interactive)
  (if (equal major-mode 'dired-mode)
      (emms-add-dired)
    (call-interactively 'emms-add-file)))

;;;;;;;;;;;;;;;;;;;;;;;;;B;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun emms-playlist-new-with-name (&optional name) "" (interactive "sName: ")
  (save-excursion
    (emms-playlist-new (concat "*Music*<" name ">"))))

(defun emms-playlist-open-with-name (&optional name) "" (interactive "sName: ")
  (save-excursion
    (let (plist
	  (file (read-file-name "Playlist file: " emms-source-file-default-directory))
	  (old (car (emms-playlist-buffer-list))))
      (setq plist (concat "*Music*<" name ">"))
      (emms-playlist-set-playlist-buffer (emms-playlist-new plist))
      (emms-add-playlist file)
      (emms-playlist-set-playlist-buffer (emms-playlist-new old)) )))

(defun emms-playlist-kill-it () "" (interactive)
  (save-excursion
    (emms-metaplaylist-mode-goto-current)
    (kill-buffer (current-buffer))
    (emms-metaplaylist-mode-go)))



(define-key emms-metaplaylist-mode-map "+" 'emms-playlist-new-with-name)
(define-key emms-metaplaylist-mode-map "-" 'emms-playlist-kill-it)
(define-key emms-metaplaylist-mode-map "o" 'emms-playlist-open-with-name)
(define-key emms-metaplaylist-mode-map "g" 'emms-metaplaylist-mode-go)
(define-key emms-browser-mode-map "g" 'emms-browser-regenerate)


;; Key Bindings ... m stands for Music.
(define-prefix-command 'my-music-map)
(define-key my-prefix-map "m" 'my-music-map)

(define-key my-music-map "b" 'emms-smart-browse)
(define-key my-music-map "l" 'emms-metaplaylist-mode-go)
(define-key my-music-map "o" 'emms-add-playlist)
(define-key my-music-map "a" 'emms-my-add)
(define-key my-music-map "s" 'emms-start)
(define-key my-music-map "S" 'emms-stop)
(define-key my-music-map "P" 'emms-pause)
(define-key my-music-map "n" 'emms-next)
(define-key my-music-map "p" 'emms-previous)
(define-key my-music-map "t" 'emms-show)
(define-key my-music-map "ir" 'emms-info-repeating)
(define-key my-music-map "it" 'emms-info-current-track) ;; TODO show the tags
(define-key my-music-map "R" 'emms-toggle-repeat-playlist)
(define-key my-music-map "r" 'emms-toggle-repeat-track)
(define-key my-music-map "-" 'emms-volume-mode-minus)
(define-key my-music-map "+" 'emms-volume-mode-plus)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(emms-devel)
;(emms-default-players)
(emms-mode-line 0)
(emms-playing-time-disable-display)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Temporarly here ...
(load-file (concat (expand-file-name user-emacs-directory) "gstreamer-el/gst-inspect.el"))
(load-file (concat (expand-file-name user-emacs-directory) "gstreamer-el/gst-various.el"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-emms)
