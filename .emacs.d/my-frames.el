;;;; Study dframe by Eric.
;; How to manage frames to display buffers.
(setq display-buffer-reuse-frames t)
;; (setq pop-up-frames nil) <<--- this setted at true, is deleterio.
;(setq special-display-buffer-names '("RMAIL" "*IBuffer*" "*Proced*"))
(setq same-window-regexps nil)
(setq special-display-regexps
      '(("\\*info.*\\*" (name ."*Info*")(width . 80)(height . 36)(unsplittable . t))
	("\\*WoMan .*\\*" (name ."*WoMan*")(width . 80)(height . 36)(unsplittable . t))
;	("\\*-jabber-\\*" (name "Jabber")(width . 30)(hight .30)(unspliitable . t))
	"\\*Group\\*" "\\*Customize "
	".*\\.freenode.*"
	"irc\\..*:.*"))

;; (progn
;;   (select-frame
;;    (make-frame '((name . "TEST")(minibuffer . nil))))
;;   (rmail))



;(string-match "\\*info\\*" (buffer-name (get-buffer "*info*")))
;;;;
;; Which frame is visible by name
(defun frame-is-visible (regex-for-name)
  ""
  (let ((s regex-for-name) (ret nil))
    (mapcar
     '(lambda (f) 
	(if (string-match s (get-frame-name f)) (setq ret f)))
     (frame-list))
    (and ret (frame-visible-p ret))))


;; Manage Frames
(eval-after-load 'my-elisp
  (progn
    (if (not (featurep 'frame-fns))
	(load-file (expand-file-name (concat user-elisp-directory "frame-fns.el"))))
    (if (not (featurep 'frame-cmds))
	(load-file (expand-file-name (concat user-elisp-directory "frame-cmds.el"))))
;;;
;; Code here.
    ;; for resizing ... locked at top-left corner.
    (defvar frame-resizing-mode-map
      (let ((m (make-sparse-keymap)))
	(define-key m (kbd "<up>") 'shrink-frame)
	(define-key m (kbd "<down>") 'enlarge-frame)
	(define-key m (kbd "<left>") 'shrink-frame-horizontally)
	(define-key m (kbd "<right>") 'enlarge-frame-horizontally)
	(define-key m (kbd "C-c C-c") 'frame-resizing-mode)
;	(define-key m (kbd "RET") 'frame-resizing-mode)
	m))

;;;###autoload
    (define-minor-mode frame-resizing-mode
      :initial-value nil
      :ligther " F-Resizing "
      :keymap 'frame-resizing-mode-map)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;; for positioning ... locked at top-left corner.
    (defvar frame-positioning-mode-map
      (let ((m (make-sparse-keymap)))
	(define-key m (kbd "<up>") 'move-frame-up)
	(define-key m (kbd "<down>") 'move-frame-down)
	(define-key m (kbd "<left>") 'move-frame-left)
	(define-key m (kbd "<right>") 'move-frame-right)
	(define-key m (kbd "C-c C-c") 'frame-positioning-mode)
;	(define-key m (kbd "RET") 'frame-positioning-mode)
	m))

;;;###autoload
    (define-minor-mode frame-positioning-mode
      :initial-value nil
      :ligther " F-Positioning "
      :keymap 'frame-positioning-mode-map)

    (defun maximum-frame-height () ""
      (- (/ (x-display-pixel-height)(frame-char-height)) 1))
    (defun maximum-frame-width () ""
      (- (/ (- (x-display-pixel-width)
	       (frame-parameter (selected-frame) 'left-fringe)
	       (frame-parameter (selected-frame) 'right-fringe)) (frame-char-width)) 2))

    (defun set-unmaximized-frame-sizes () "" (interactive)
      (set-frame-parameter (selected-frame) 'unmaximized-width
			   (frame-parameter (selected-frame) 'width))
      (set-frame-parameter (selected-frame) 'unmaximized-height
			   (frame-parameter (selected-frame) 'height)))

    (defun un/maximize-frame (&optional frame)""(interactive)
      (unless (framep frame) (setq frame (selected-frame)))
      (if (frame-parameter frame 'maximized)
	  (progn
	    (set-frame-size frame 
			    (or (frame-parameter frame 'unmaximized-width) 80)
			    (or (frame-parameter frame 'unmaximized-height) 32))
	    (set-frame-parameter frame 'maximized nil))
	(set-frame-size frame (maximum-frame-width) (maximum-frame-height))
	(set-frame-parameter frame 'maximized t)))
	
	    
;;;     (defun maximize-frame (&optional frame)""(interactive)
;;;       (if (not (framep frame)) (setq frame (selected-frame))))

;;;      (defun unmaximize-frame (&optional frame)""(interactive)
;;;       (if (not (framep frame)) (setq frame (selected-frame)))
;;;       (set-frame-size frame unmaximized-frame-width unmaximized-frame-height)
;;;       (set-frame-parameter frame 'maximized nil))

;; Code End here.
    "My Frames Managment loaded."))

(provide 'my-frames)
