;(require 'w3) ;;  W3 sucks
(defun select-browser-web ()
  ""
  (interactive)
  (let ((browser (intern (completing-read "Select a Web Browser: " '((w3m) (mozilla)) nil))))
    (cond ((eq browser 'w3m)(setq browse-url-browser-function 'w3m-browse-url))
	  ((eq browser 'mozilla)(setq browse-url-browser-function 'browse-url-firefox))
	  (t (setq browse-url-browser-function 'browse-url-default-browser)))
    (message "You'll use the %s browser." (symbol-name browser))))


; browse-url-default-browser
; (setq browse-url-browser-function 'browse-url-mozilla)
;(require 'browse-url)
(autoload 'ewb "elisp/ewb.el" "emacs web browser" t)

;; (require 'w3m-load)
;; (require 'w3m-cookie)
;;(setq browse-url-browser-function 'w3m-browse-url)
;(setq browse-url-browser-function nil)
;(setq browse-url-of-buffer 'w3m-browse-url)

;;;; W3M Customozation
;w3m-mode-hook
;(w3m-fb-mode t)

(provide 'my-browse-url)
