;;;;;;;;;;;;;;;;;;; -*- folded-file: t; -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;{{{ Loading-utils Functions for elisp  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar user-elisp-directory
  (concat user-emacs-directory "elisp/")
  "")

(defun use-dotElisp (&optional feature) "" (interactive)
  (let ((_file (if (not feature)
		      (read-file-name "what to load?" user-elisp-directory)
		 (expand-file-name
		  (concat user-elisp-directory feature ".el")))))    
    (if (file-exists-p _file)
	(load-file _file)
      (message "What ???"))))

(defun check-provided (&optional feature in-file)
  "Check for provide calls."
  (let ((_feature (if (not feature) ".*"
		    feature))
	(_infile (if (not in-file) "*.el"
		   in-file)) result (retval '()))
    (save-excursion
      (cd user-emacs-directory)
      (setq result
	    (split-string
	     (shell-command-to-string
	      (concat "grep -nH -e \"^(provide '" _feature ")$\" elisp/" _infile)) "\n" t))
;      (print result)
      (dolist (line result)
	;      (print line)
	(setq line (split-string line ":" t))
	;      (print line)
	(add-to-list 'retval (cons (substring (nth 2 line) 10 -1)(basename (nth 0 line)))))
;	(print retval)))))
	retval)))

;;;; Macro to require libs in elisp/ easely
(defmacro require-or-load-elisp-library (feature)
  "Require a feature in elisp user dir by name."
  `(let ((ext
	  (if (file-exists-p
	       (expand-file-name
		(concat user-elisp-directory (symbol-name (quote ,feature)) ".elc"))) ".elc" ".el")))
     ;(unless (not (or nil (quote ,wrap)))
     ; (add-to-list 'load-path user-elisp-directory))
     (if (assq (quote ,feature)
	       (check-provided
		(symbol-name (quote ,feature))
		(concat (symbol-name (quote ,feature)) ".el")))
	 (require (quote ,feature)
		  (expand-file-name
		   (concat user-elisp-directory (symbol-name (quote ,feature)) ext)) t)
       (load
	(expand-file-name
	 (concat user-elisp-directory (symbol-name (quote ,feature)) ext)) t))
     ;     (unless (not (or nil (quote ,wrap)))
     ;       (setq load-path (remove user-elisp-directory load-path))))
     (featurep (quote ,feature))))

(defmacro require-or-load-elisp-library-wrapped (feature)
  "Require a feature in elisp user dir by name."
  `(let ((ext
	  (if (file-exists-p
	       (expand-file-name
		(concat user-elisp-directory (symbol-name (quote ,feature)) ".elc"))) ".elc" ".el")))
     ;(unless (not (or nil (quote ,wrap)))
     ; (add-to-list 'load-path user-elisp-directory))
     (add-to-list 'load-path user-elisp-directory)
     (if (assq (quote ,feature)
	       (check-provided
		(symbol-name (quote ,feature))
		(concat (symbol-name (quote ,feature)) ".el")))
	 (require (quote ,feature)
		  (expand-file-name
		   (concat user-elisp-directory (symbol-name (quote ,feature)) ext)) t)
       (load
	(expand-file-name
	 (concat user-elisp-directory (symbol-name (quote ,feature)) ext)) t))
     ;     (unless (not (or nil (quote ,wrap)))
     (setq load-path (remove user-elisp-directory load-path))
     (featurep (quote ,feature))))

(defun require-or-load-elisp-library-fn (afeature &optional wrap)
  "Require a feature in elisp user dir by name."
  (let ((ext
	 (if (file-exists-p
	      (expand-file-name
	       (concat user-elisp-directory (symbol-name afeature) ".elc"))) ".elc" ".el")))
    (unless (not wrap)
      (add-to-list 'load-path user-elisp-directory))
     (if (assq afeature
	       (check-provided
		(symbol-name afeature)
		(concat (symbol-name afeature) ".el")))
	 (require afeature
		  (expand-file-name
		   (concat user-elisp-directory (symbol-name afeature) ext)) t)
       (load
	(expand-file-name
	 (concat user-elisp-directory (symbol-name afeature) ext)) t))
     (unless (not wrap)
       (setq load-path (remove user-elisp-directory load-path))))
  (featurep afeature))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar lines-begin-regexp
  "^;; *Features .*required by this library:$"
  "regexp for meaning lines")

(defvar lines-end-regexp
  "^;;;;+$"
  "regexp for meaning lines")

(defvar features-regexp "`\\([-a-z+0-9]*\\)'[,.]" "")

(defun requiredBy-ed (lib &optional grep-features-function)
  "return a list"
  (save-excursion
    (unless (file-exists-p lib) (return nil))
    (let ((buf (find-file-noselect lib))
	  (rbuf (generate-new-buffer "reqBy"))
	  pos (ret '()))
      (progn
	(set-buffer buf) (beginning-of-buffer)
	(if (functionp grep-features-function)
	    (return grep-features-function (buffer-string)))
	(setq pos (string-match lines-begin-regexp (buffer-string)))
	(if (not pos) (return))
	(goto-char pos)(set-mark (point))
	(setq pos (string-match lines-end-regexp (buffer-string)))
	(if (not pos) (return))
	(goto-char pos)
	(copy-to-buffer rbuf (mark) (point))
	(kill-buffer buf)
	(set-buffer rbuf)
	(setq pos (string-match features-regexp (buffer-string) 0))
;	(print (match-string 0))
	(while (numberp pos)
	  (add-to-list 'ret (match-string-no-properties 1 (buffer-string)) t)
	  (setq pos (match-end 1))
	  (setq pos (string-match features-regexp (buffer-string) pos)))
	(kill-buffer (current-buffer))
	ret))))

(defun requiredBy (sym) "return a list"
  (let ((file
	 (expand-file-name
	  (concat user-elisp-directory (symbol-name sym) ".el"))))
    (if (not (file-exists-p file))
	(list (symbol-name sym))
      (remove (symbol-name sym) (requiredBy-ed file)))))


(defun require-all-inlist (list) ""
  (dolist (lib list nil)
    (let ((file (expand-file-name
		 (concat user-elisp-directory lib ".el")))
	  (sym (intern lib)))
      (if (file-exists-p file) (require-all-inlist (requiredBy sym)))
      (require sym
	       (if (file-exists-p file) file nil) t))))

(defun require-all-for (pkg) ""
  (let ((file
	 (expand-file-name
	  (concat user-elisp-directory (symbol-name pkg) ".el"))))
    (require-all-inlist (requiredBy pkg))
    (require pkg
	     (if (file-exists-p file) file nil) t)))
    
;(require-all-for ')

;;}}}

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; my-require-pkg ...
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;(require-all-for 'start) ;; by Drew Adams
;; ;; Prevent dired+ font-lock-decoration ...
;; (setq font-lock-maximum-decoration nil)

;;;; pkg: doremi-pkg
(defun my-require-doremi-pkg () "" (interactive)
  (progn
;;     (require-all-for 'imenu+)
    (require-or-load-elisp-library-wrapped imenu+)
;;     (require-all-for 'autofit-frame)
    (require-or-load-elisp-library-wrapped autofit-frame)
;;     (require-all-for 'thumb-frm)
    (require-or-load-elisp-library-wrapped thumb-frm)
;;     (require-all-for 'zoom-frm)
    (require-or-load-elisp-library-wrapped zoom-frm)
;;     (require-all-for 'doremi) ;; Dynamic adjustment of frame properties.
    (require-or-load-elisp-library-wrapped doremi)
;;     ;; Loads `doremi.el'.
;;     (require-all-for 'doremi-cmd) ;; Other Do Re Mi commands.
    (require-or-load-elisp-library-wrapped doremi)
;;     (require-all-for 'doremi-frm) ;; Dynamic adjustment of frame properties.
    (require-or-load-elisp-library-wrapped doremi)
;;     (require-all-for 'doremi-mac) ;; Do Re Mi Macro.
    (require-or-load-elisp-library-wrapped doremi)
    'my-doremi-pkg))

;;;; pkg: grep-pkg
(defun my-require-grep-pkg () "" (interactive)
  (progn
;;     (require 'grep-edit
;; 	     (expand-file-name "~/.emacs.d/elisp/grep-edit.el")
;; 	     t)                   
    (require-or-load-elisp-library grep-edit)
;;     (require 'grep-buffers
;; 	     (expand-file-name "~/.emacs.d/elisp/grep-buffers.el")
;; 	     t)
    (require-or-load-elisp-library grep-buffers)
    'my-grep-pkg))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;; Dired Stuff ...
(defun my-require-dired-details-pkg () "" (interactive)
  (progn
;;     (require 'dired-details
;; 	     (expand-file-name
;; 	      "~/.emacs.d/elisp/dired-details.el") t)
    (require-or-load-elisp-library dired-details)
;;     (require 'dired-details+
;; 	     (expand-file-name
;; 	      "~/.emacs.d/elisp/dired-details+.el") t)
    (require-or-load-elisp-library dired-details+)
    'my-dired-details-pkg))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; pkg: base
(require-or-load-elisp-library strings)
(require-or-load-elisp-library ebs)
(require-or-load-elisp-library bar-cursor)
(require-or-load-elisp-library perl-find-library)
(require-or-load-elisp-library modeline-posn)

(if (< emacs-major-version 22)
    (require 'compile+20 nil t)			; Highlighting, etc.
  (require 'compile-
	   (expand-file-name "~/.emacs.d/elisp/compile-.el")
	   t)             	; Highlighting.
  (require 'compile+
	   (expand-file-name "~/.emacs.d/elisp/compile+.el")
	   t)             	; Highlighting.
  (require 'grep+ 
	   (expand-file-name "~/.emacs.d/elisp/grep+.el")
	   t))               	; Highlighting.

(require-or-load-elisp-library menu-bar+)
(require-or-load-elisp-library bookmark+)
;(require-or-load-elisp-library dired-x)
(require-or-load-elisp-library ffap-)
(my-require-dired-details-pkg)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; for manage pages from emacswiki.org 
(require-or-load-elisp-library oddmuse)
(require-or-load-elisp-library install-elisp)
(require-or-load-elisp-library require-or-install)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Customize elisp code from EmacsWiki
(setq wikiarea-managed-directory user-elisp-directory)
(setq install-elisp-repository-directory user-elisp-directory)
;; to edit the emacswiki.org
(add-hook 'oddmuse-mode-hook
	  (lambda ()
	    (unless (string-match "question" oddmuse-post)
	      (setq oddmuse-post (concat "uihnscuskc=1;" oddmuse-post)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Stuff
;;;;;;;;;;;;;;;;
;;;; for Google Account
(require-or-load-elisp-library googleaccount)
;;;; for Google search
(require-or-load-elisp-library google-define)
;;;; for GObject coding
(require-or-load-elisp-library gobject-class)
;;;; for elisp code from emacswiki
(require-or-load-elisp-library wikiarea-fixed)
;;;; for Texi
(require-or-load-elisp-library eval-to-texi)
;;;; for Babel Fish on the Web
(require-or-load-elisp-library babel)
;;;; for Color Browser
(if window-system (require-or-load-elisp-library color-browser))
;;;; for Color Theme Maker
(require-or-load-elisp-library color-theme-maker)
;;;; for control registers
(require-or-load-elisp-library register-list)
;;;; for finding pkgs (finder C-h p)
;(require-or-load-elisp-library finder+)
;;;; for inspection with `xray'
;; is so important the compiled ??
;(load (expand-file-name (concat user-elisp-directory "xray.elc")))
;(require-or-load-elisp-library xray)
(require-or-load-elisp-library doc-view)
;;;; for Emacs Image Manipulation Program
(require-or-load-elisp-library eimp)
;(require 'eimp (expand-file-name (concat user-elisp-directory "eimp.elc")))
;;;; for Easy HyperText Navigation. (it's fantastic ;)
(require-or-load-elisp-library linkd)
;(require-or-load-elisp-library-fn 'icicles t)
(require-or-load-elisp-library-wrapped icicles)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-elisp)
