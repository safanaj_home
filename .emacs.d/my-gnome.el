;;; GNOME Tools Wrapper Library
(define-async-job-or-service-w/o-args gnome-terminal)

(define-async-job-or-service-w/o-args gnome-wm)
(define-async-job-or-service-w/o-args gnome-panel)
(define-async-job-or-service-w/o-args gnome-session)


(provide 'my-gnome)