;;;; Writing ... use Muse
(require 'muse-mode)     ; load authoring mode

(require 'muse-html)     ; load publishing styles I use
(require 'muse-latex)
(require 'muse-texinfo)
(require 'muse-docbook)
(require 'muse-wiki)

(require 'muse-project)
(setq muse-project-alist ;; nil)
      '(("website"			; my various writings
	 ("~/Pages" :default "idx")
	 (:base "xhtml" :path "~/Pages/.Web") )
;;	 (:base "info" :path "~/Pages/.Infos")

	("WikiPlanner"
	 ("~/Pages/Plans" :default "Tesi")
	 (:base "xhtml" :path "~/Pages/.Web"))))

(add-hook 'find-file-hooks 'muse-mode-maybe)
(defun find-pages () "" 
  (interactive) (muse-project-find-file "idx" "website"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Remember in text (~/.emacs.d/.notes)
;; (require 'remember nil t)
;; (setq remember-data-file (expand-file-name (concat user-emacs-directory "/.notes")))
;; ;(setq remember-handler-functions '(remember-append-to-file)) ; for Org
;; (setq remember-annotation-functions '(org-remember-annotation))
;; (setq remember-handler-functions '(org-remember-handler))
;; ;(add-hook 'remember-mode-hook 'org-remember-apply-template) ;; non esageriamo

;; For Planner
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(require 'remember-planner)
;(setq remember-handler-functions '(remember-planner-append))
;(defvaralias 'remember-annotation-functions 'planner-annotation-functions)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(setq planner-directory "~/Pages/Plans")
;; (setq planner-use-day-pages nil)
;; (require 'planner)
;; (require 'planner-rmail)
;; ;; (require 'planner-diary)
;; ;; (add-hook 'diary-display-hook 'fancy-diary-display)
;; ;; (setq planner-diary-use-diary t)
;; ;; (planner-diary-insinuate)
;; ;; (planner-insinuate-calendar)

;; (require 'planner-lisp)
;; ;; (require 'planner-timeclock)
;; ;; (require 'planner-timeclock-summary)

;; (if (featurep 'muse-mode)
;;     (add-hook 'after-save-hook
;; 	      '(lambda ()
;; 		 (when (planner-derived-mode-p 'muse-mode)
;; 		   (muse-project-publish nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'my-pages)