# ~/.bash_profile: executed by bash(1) for login shells.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/login.defs
umask 022
unset LC_ALL LANG LANGUAGE
LC_ALL="C"
LANG=it_IT.utf8
LANGUAGE=it_IT.utf8

export ___BASH_PROFILE_IS_EXPORTED=yes

# include .bashrc if it exists
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi
# echo 'sourced .bashrc '

PATH=/sbin:/usr/sbin:/gnu/bin:"${PATH}"
MANPATH=/usr/share/man:/usr/local/share/man:/gnu/share/man
INFOPATH=/gnu/share/info:/usr/share/info:/usr/local/share/info

GS_SETUP=~/GNUstep/.gs-setup
[ -f ${GS_SETUP} ] && . ${GS_SETUP}
# echo 'sourced .gs-setup '
## Not in .GNUstep.conf because is not a standard variable. (gnustep-config ugly it)
GNUSTEP_USER_MAKEFILES=`gnustep-config --variable=GNUSTEP_USER_LIBRARY`/Makefiles
export GNUSTEP_USER_MAKEFILES

# # for GCC snapshot
# LD_LIBRARY_PATH=/usr/lib/gcc-snapshot/lib:$LD_LIBRARY_PATH
# PATH=/usr/lib/gcc-snapshot/bin:$PATH


#LYNX_CFG=~/lynx.cfg

# set PATH so it includes user's private bin if it exists
if [ -d ~/bin ] ; then
    PATH=~/bin:"${PATH}"
fi


# For editing
EDITOR="run_emacs"
ALTERNATE_EDITOR="emacs"

export PATH LANG MANPATH INFOPATH EDITOR ALTERNATE_EDITOR

if [ -z "$DISPLAY" ]; then
    echo 'in X ricordati del prompt !! (PROMPT_COMMAND=)'
fi

CVS_RSH=ssh
export CVS_RSH
