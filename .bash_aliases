alias cls='clear'
alias cl='cls;ll -CF'
alias dir='ls --color=auto --format=vertical'
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -lah'
alias ls='ls --color=auto'
alias pu='pushd'
alias po='popd'
alias man='man -L it'
alias vdir='ls --color=auto --format=long'
alias vime='vim -q /tmp/vimErr.err'
alias se='sensible-editor'
alias p='sensible-pager'
alias h='head'
alias g='grep'
alias x='xargs'
alias x0='xargs -0'
alias xg='xargs grep'
alias xc='xargs cat'
alias xp='xargs pager'
alias xf='xargs file'
alias xwc='xargs wc'
alias i='info'
alias j='jobs'
alias psh='ps -H'

alias sudeq='sudo emacs -Q -nw'

alias sshunibo='ssh -i .ssh/IP6_id_dsa bardelli@crown.cs.unibo.it'

alias l1='ls -1'

####for Emacs
alias eq='emacs -Q -nw'
alias e='emacs -Q'
alias ec='emacsclient'
alias RMAIL='emacs -Q -f rmail'



